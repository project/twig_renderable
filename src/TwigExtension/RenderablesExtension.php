<?php

namespace Drupal\twig_renderable\TwigExtension;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\Attribute;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * RenderablesExtension class.
 */
class RenderablesExtension extends AbstractExtension {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs \Drupal\twig_renderable\TwigExtension\RenderablesExtension.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'twig_renderable';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('will_have_output', [$this, 'willHaveOutput'], [
        'needs_context' => TRUE,
        'needs_environment' => TRUE,
        'is_variadic' => TRUE,
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('add_class', [$this, 'addClass']),
      new TwigFilter('merge_attributes', [$this, 'mergeAttributes']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addClass($renderable, $class) {
    if (is_array($renderable)) {
      $renderable['#attributes']['class'][] = $class;
    }
    elseif ($renderable instanceof Attribute) {
      $renderable->addClass($class);
    }

    return $renderable;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeAttributes($renderables, $attributes) {
    if ($attributes instanceof Attribute) {
      $attributes = $attributes->toArray();
    }

    if (is_array($renderables)) {
      if (isset($renderables['#attributes'])) {
        $renderables['#attributes'] = array_merge_recursive($renderables['#attributes'], $attributes);
      }
      else {
        $renderables['#attributes'] = $attributes;
      }
    }
    elseif ($renderables instanceof Attribute) {
      if (isset($attributes['class'])) {
        $renderables->addClass($attributes['class']);
        unset($attributes['class']);
      }
      foreach ($attributes as $name => $value) {
        $renderables->setAttribute($name, $value);
      }
    }

    return $renderables;
  }

  /**
   * {@inheritdoc}
   */
  public function willHaveOutput(Environment $env, array &$context, $variable, array $parents = []) {
    array_unshift($parents, $variable);
    $key_exists = NULL;
    $element = &NestedArray::getValue($context, $parents, $key_exists);

    if (!$key_exists || $element === NULL) {
      return FALSE;
    }

    if (is_array($element)) {
      $output = $this->renderer->render($element);
      $element = [
        '#markup' => $output,
      ];

      if ($env->isDebug()) {
        $output = preg_replace('/<!--(.*)-->/Uis', '', $output);
      }

      return trim($output) != '';
    }
    else {
      return trim($element) != '';
    }
  }

}
